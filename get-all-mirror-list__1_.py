#!/usr/bin/python
import sys
import csv
import json
import requests
from time import sleep


wrfile = "output.txt"
out = open(wrfile, 'a')

url = "https://gitlab.axiatadigitallabs.com/api/v4/projects?per_page=100&page=1"
headers = {"content-type": "application/json", "Accept-Charset": "UTF-8", "PRIVATE-TOKEN": "cPxqocKBWTGnSRqNytHa"}
res = requests.get(url, headers=headers)
data = res.json()
pages = res.headers["X-Total-Pages"]

for page in range(1,int(pages)+1,1):
    print(page)
    url = "https://gitlab.axiatadigitallabs.com/api/v4/projects?per_page=100&page="+str(page)
    print(url)
    headers = {"content-type": "application/json", "Accept-Charset": "UTF-8", "PRIVATE-TOKEN": "cPxqocKBWTGnSRqNytHa"}
    res = requests.get(url, headers=headers)
    data = res.json()

    for x in data:
        projectId = str(x['id'])
        #projectId = x['id']
        projectName = x['name']
        webURL = x['web_url']
        #print(projectName)
        url1 = "https://gitlab.axiatadigitallabs.com/api/v4/projects/" + projectId + "/remote_mirrors"
        headers = {"content-type": "application/json", "Accept-Charset": "UTF-8", "PRIVATE-TOKEN": "cPxqocKBWTGnSRqNytHa"}
        res = requests.get(url1, headers=headers)
        data2 = res.json()
        sleep(0.05)

        for y in data2:
            out.write(projectId)
            out.write(", ")
            out.write(projectName)
            out.write(", ")
            out.write(webURL)
            out.write(", ")
            out.write(y['url'])
            out.write("\n")
